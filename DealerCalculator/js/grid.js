﻿function calculate() {
    var vcost = inputCost.value;
    var vturn = inputTurn.value;
    document.getElementById("acharge").textContent = 'Auction Source Total Charge ' + vturn + ' Days Funding';
    document.getElementById("tcharge").textContent = 'Trade Source Total Charge ' + vturn + ' Days Funding';
    setContent();
    // Tier 1 Load fee
    if (vcost <= 4000) {
        document.getElementById("a1").textContent = 35;
        document.getElementById("t1").textContent = 55;
    }
    // Tier 2 load fee & Tier 2 Settlement fee
    if ((vcost >= 4001) && (vcost < 10001)) {
        document.getElementById("a2").textContent = 65;
        document.getElementById("t2").textContent = 85;
        document.getElementById("a7").textContent = 30;
        document.getElementById("t7").textContent = 30;
    }
    // Tier 3 load fee & Tier 3 settlement fee
    if (vcost >= 10001) {
        document.getElementById("a3").textContent = 85;
        document.getElementById("t3").textContent = 99;
        document.getElementById("a8").textContent = 40;
        document.getElementById("t8").textContent = 40;
    }
    // Day 45
    if (vturn >= 45) {
        document.getElementById("a4").textContent = 15;
        document.getElementById("t4").textContent = 15;
    }
    // Day 75
    if (vturn >= 75) {
        document.getElementById("a5").textContent = 15;
        document.getElementById("t5").textContent = 15;
    }
    // Day 105
    if (vturn >= 105) {
        document.getElementById("a6").textContent = 15;
        document.getElementById("t6").textContent = 15;
    }
    // Total holding charge
    var totCharge = calcHoldingCharge(vcost, vturn);

    document.getElementById("a9").textContent = displayPound(totCharge, 2);
    document.getElementById("t9").textContent = displayPound(totCharge, 2);
    var auctionTotal = getAuctionCharge();
    afund.textContent = displayPound(auctionTotal, 2);
    var tradeTotal = getTradeCharge();
    tfund.textContent = displayPound(tradeTotal, 2);

    // Payment due
    var due45 = (7.5 / 100) * vcost;
    var due75 = (10 / 100) * vcost;
    var due105 = (10 / 100) * vcost;
    document.getElementById("day45").textContent = displayPound(due45, 2);
    document.getElementById("day75").textContent = displayPound(due75, 2);
    document.getElementById("day105").textContent = displayPound(due105, 2);
    return false;
}
function calcHoldingCharge(cost, turn) {
    // First calculate the payments
    var pay1 = (7.5 / 100) * cost;
    var pay2 = (10.0 / 100) * cost;
    var pay3 = (10.0 / 100) * cost;
    // Now calculate the values after payments
    var orig = cost;
    var after45 = orig - pay1;
    var after75 = after45 - pay2;
    var after105 = after75 - pay3;
    // Now calculate the rates
    var rate1 = dailyCharge(orig);
    var rate2 = dailyCharge(after45);
    var rate3 = dailyCharge(after75);
    var rate4 = dailyCharge(after105);
    // Display the rates
    charge1.textContent = displayPound(rate1, 2);
    charge2.textContent = displayPound(rate2, 2);
    charge3.textContent = displayPound(rate3, 2);
    charge4.textContent = displayPound(rate4, 2);
    // Now calculate the total charge
    var totCharge = holdingCharge(turn, rate1, rate2, rate3, rate4);
    return totCharge;
}
function dailyCharge(cost) {
    var rate = (cost / 100) * 11.0;
    rate = rate / 365;
    return rate;
}
function setContent() {
    document.getElementById("a1").textContent = 0;
    document.getElementById("a2").textContent = 0;
    document.getElementById("a3").textContent = 0;
    document.getElementById("a4").textContent = 0;
    document.getElementById("a5").textContent = 0;
    document.getElementById("a6").textContent = 0;
    document.getElementById("a7").textContent = 0;
    document.getElementById("a8").textContent = 0;
    document.getElementById("a9").textContent = 0;
    document.getElementById("t1").textContent = 0;
    document.getElementById("t2").textContent = 0;
    document.getElementById("t3").textContent = 0;
    document.getElementById("t4").textContent = 0;
    document.getElementById("t5").textContent = 0;
    document.getElementById("t6").textContent = 0;
    document.getElementById("t7").textContent = 0;
    document.getElementById("t8").textContent = 0;
    document.getElementById("t9").textContent = 0;
    document.getElementById("afund").textContent = 0;
    document.getElementById("tfund").textContent = 0;
}
function getAuctionCharge() {
    var tot = 0;
    tot = tot + roundTo(document.getElementById("a1").innerHTML, 2);
    tot = tot + roundTo(document.getElementById("a2").innerHTML, 2);
    tot = tot + roundTo(document.getElementById("a3").innerHTML, 2);
    tot = tot + roundTo(document.getElementById("a4").innerHTML, 2);
    tot = tot + roundTo(document.getElementById("a5").innerHTML, 2);
    tot = tot + roundTo(document.getElementById("a6").innerHTML, 2);
    tot = tot + roundTo(document.getElementById("a7").innerHTML, 2);
    tot = tot + roundTo(document.getElementById("a8").innerHTML, 2);
    var fund = roundTo(removePound(document.getElementById("a9").innerHTML, 2));
    tot = tot + fund;
    return tot;
}
function getTradeCharge() {
    var tot = 0;
    tot = tot + roundTo(document.getElementById("t1").innerHTML, 2);
    tot = tot + roundTo(document.getElementById("t2").innerHTML, 2);
    tot = tot + roundTo(document.getElementById("t3").innerHTML, 2);
    tot = tot + roundTo(document.getElementById("t4").innerHTML, 2);
    tot = tot + roundTo(document.getElementById("t5").innerHTML, 2);
    tot = tot + roundTo(document.getElementById("t6").innerHTML, 2);
    tot = tot + roundTo(document.getElementById("t7").innerHTML, 2);
    tot = tot + roundTo(document.getElementById("t8").innerHTML, 2);
    var fund = roundTo(removePound(document.getElementById("t9").innerHTML, 2));
    tot = tot + fund;
    return tot;
}
function roundTo(value, exp) {
    if (typeof exp === 'undefined' || +exp === 0)
        return Math.round(value);

    value = +value;
    exp = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
        return NaN;

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}
function displayPound(outVal, precision) {
    var outdisp = '£' + currencyFormatted(roundTo(outVal, precision));
    return outdisp;
}
function removePound(inText) {
    var outText = inText.substr(1);
    return outText.trim();
}
function currencyFormatted(amount) {
    var i = parseFloat(amount);
    if (isNaN(i)) { i = 0.00; }
    var minus = '';
    if (i < 0) { minus = '-'; }
    i = Math.abs(i);
    i = parseInt((i + .005) * 100);
    i = i / 100;
    s = new String(i);
    if (s.indexOf('.') < 0) { s += '.00'; }
    if (s.indexOf('.') == (s.length - 2)) { s += '0'; }
    s = minus + s;
    return s;
}
function holdingCharge(turn, rate1, rate2, rate3, rate4) {
    var totCharge = 0.00;
    var part1;
    var part2;
    var part3;
    var part4;
    var avRate;
    if (turn < 45) {
        totCharge = turn * rate1;
        avRate = rate1;
        charge5.textContent = displayPound(avRate, 2);
        return totCharge;
    }
    if (turn < 75) {
        part1 = 44 * rate1;
        part2 = (turn - 44) * rate2;
        totCharge = part1 + part2;
        avRate = (rate1 + rate2) / 2;
        charge5.textContent = displayPound(avRate, 2);
        return totCharge;
    }
    if (turn < 105) {
        part1 = 44 * rate1;
        part2 = 30 * rate2;
        part3 = (turn - 74) * rate3;
        totCharge = part1 + part2 + part3;
        avRate = (rate1 + rate2 + rate3) / 3;
        charge5.textContent = displayPound(avRate, 2);
        return totCharge;
    }
    if (turn >= 105) {
        part1 = 44 * rate1;
        part2 = 30 * rate2;
        part3 = 30 * rate3;
        part4 = (turn - 104) * rate4;
        totCharge = part1 + part2 + part3 + part4;
        avRate = (rate1 + rate2 + rate3 + rate4) / 4;
        charge5.textContent = displayPound(avRate, 2);
        return totCharge;
    }
    return totCharge;
}