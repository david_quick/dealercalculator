# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
HTML page for the dealer calculator

* Quick summary
Sales execs will use the calculator to indicate costs to a dealer
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
Deployment of stand alone HTML page via Pardot
* Configuration
None required
* Dependencies
None
* Database configuration
None
* How to run tests
Compare values from internal spreadsheet to HTML page
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###
david quick - david.quick@nextgearcapital.co.uk
* Repo owner or admin
Kimberley Shone at NextGear Capital UK
* Other community or team contact